$(function () {

    //scroll
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top - 90
                }, 600);
                return false;
            }
        }
    });

    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    });
	
	$(".btn_super_drops_enter ").click(function () {
        $('.popup').fadeIn();
    });
	
	$(".popup_close").click(function () {
        $('.popup').fadeOut();
    });
})
