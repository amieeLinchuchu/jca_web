$(function() {
  /**
   * 手機版【belif獨門】輪播
   **/ 
  var edelweissSlide = new Slide({
    indicatorSelectors: '.index_carousel_wrap .index_carousel_indicators .item',
    itemSelectors: '.edelweiss_product_make .edelweiss_make_contain .edelweiss_make_item',
    // mode: 'opacity',
    duration: 5000,
    isAutoPlay: true
  });
});
