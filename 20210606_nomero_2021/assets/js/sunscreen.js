$(function() {   
    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    }); 
	
	$(".suncreen_btn_enter").click(function () {
        $('.popup').fadeIn();
    });
	
	$(".popup_wrap").click(function () {
        $('.popup').fadeOut();
    });
    
})