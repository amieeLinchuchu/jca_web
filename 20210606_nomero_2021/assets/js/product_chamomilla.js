$(function() {
  /**
   * 手機版【belif獨門】蜂蜜二次萃取法輪播
   **/ 
  var chamomillaSlide = new Slide({
    indicatorSelectors: '.index_carousel_wrap .index_carousel_indicators .item',
    itemSelectors: '.chamomilla_special .chamomilla_special_contain .chamomilla_special_item',
    // mode: 'opacity',
    duration: 5000,
    isAutoPlay: true
  });


  /**
   * 點擊移動到指定單元
   **/ 
  var $tag = $('.fixed_wrap a');
  
  $tag.on('click', function (e) {
    e.preventDefault();
    var $this = $(this);

    var tag = $this.prop('href').substring($this.prop('href').indexOf('#'));
    var $content = $(tag);
    // console.log($content)
    var height = $('header').height();
    var top = ($content.offset() || {}).top;
    var top2 = $('.wrap').offset().top;

    var diduration = Math.max(top / 2, 800);
    diduration = Math.min(diduration, 1000);

    $('html, body').stop().animate({
      scrollTop: top - top2 - height
    }, {
      duration: diduration
    });

    var $content1 = $('#tag-1');
    var $dmTop = ($content1.offset() || {}).top;
  });
});
