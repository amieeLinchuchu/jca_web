$(function () {
	$(".btn_gotop img").click(function () {
		jQuery("html,body").stop(true, false).animate({
			scrollTop: 0
		}, 700); //設定回頁面頂端
		return false;
	});
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) { //設定大於300px才顯示浮層
			$('.btn_gotop,.btn_buynow').fadeIn("fast");
			$('.btn_buynow').css('display','block');
		} else {
			$('.btn_gotop,.btn_buynow').stop().fadeOut("fast");
		}
	});

	//scroll
	$('a[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 600);
				return false;
			}
		}
	});
	
	$('.btn_menu,.menu ul li a').click(function () {
		$('.menu').toggleClass('click');
	});

});
