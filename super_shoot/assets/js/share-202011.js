$(function() {
  var layera_00 = $('.share_flower-layer1').get(0);
  var layera_01 = $('.share_flower-layer2').get(0);
  var layera_02 = $('.share_flower-layer3').get(0);
  // var layera_03 = $('.about_layer-4').get(0);
  // var layera_04 = $('.about_layer-5').get(0);

  var parallaxa0 = new Parallax(layera_00, {
    invertX: false,
    invertY: false,
  });
  var parallaxa1 = new Parallax(layera_01, {});
  // var parallaxa2 = new Parallax(layera_02, {});
  // var parallaxa3 = new Parallax(layera_03, {});
  var parallaxa2 = new Parallax(layera_02, {
    invertX: false,
    invertY: false,
  });

  parallaxa0.depths = [2];
  parallaxa1.depths = [1.6];
  // parallaxa2.depths = [1.2];
  // parallaxa3.depths = [3];
  parallaxa2.depths = [0.8];
});
