$(function() {
    
    //scroll
    $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 400);
        return false;
        }
        }
    });
	
	$(".c-ct,.check-content").click(function () {
        $(".checkmark").toggleClass('active');
    });
	
	
	$(".vitawater_wrapper select").click(function () {
        $('.vitawater_wrapper select').addClass('active');
    });
	
	$(".btn_enter").click(function () {
        $('.popup').fadeIn();
    });
	$(".popup_wrap").click(function () {
        $('.popup').fadeOut();
    });
    
})
