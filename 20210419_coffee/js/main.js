$(function () {

	$(".btn_menu").click(function () {
		$(".menu").toggleClass('click');
	});

	$(".btn_close,.menu_right ul li a").click(function () {
		$('.menu').removeClass('click');
	});

	$(".btn_goTop").click(function () {
		$("html,body").animate({
			scrollTop: 0,
		}, 800);
	});

	$(window).scroll(function () {
		if ($(window).scrollTop() > 100) {
			if ($(".btn_goTop").hasClass("hide")) {
				$(".btn_goTop").toggleClass("hide");
			}
		} else {
			$(".btn_goTop").addClass("hide");
		}
	});

	//scroll
	$('a[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 900);
				return false;
			}
		}
	});

	//btn_next
	//$(".btn_next").click(function () {
	//	var nowProduct = $(".product_img div").filter(".active").index();
	//    var allProuduct = $(".product_img div").length;
	//    var TT = allProuduct - 1;
	//    
	//	if (nowProduct < TT) {
	//		nexProduct = nowProduct + 1;
	//	} else {
	//		nexProduct = TT;
	//	};
	//	$(".product_img div").eq(nexProduct).addClass("active").siblings().removeClass("active");        
	//});

	//btn_pre
	//$(".btn_pre").click(function () {
	//	var nowProduct = $(".product_img div").filter(".active").index();
	//	if (nowProduct > 0) {
	//		preProduct = nowProduct - 1;
	//	} else {
	//		preProduct = 0;
	//	};
	//	$(".product_img div").eq(preProduct).addClass("active").siblings().removeClass("active");
	//});

	//icon_click
	$('.btn_icon a').click(function () {
		var now = $(this).index();
		$(this).addClass('active').siblings().removeClass('active');

		$('.for_img div').removeClass('active').eq(now).addClass('active');

		$('.for_text div').removeClass('active').eq(now).addClass('active');
	});

	$('.btn_icon01').click(function () {
		$('.img_01').css("margin-left", "0");
	});

	$('.btn_icon02').click(function () {
		$('.img_01').css("margin-left", "-9.3%");
	});

	$('.btn_icon03').click(function () {
		$('.img_01').css("margin-left", "-18.5%");
	});

	$('.btn_icon04').click(function () {
		$('.img_01').css("margin-left", "-28.1%");
	});

	$('.btn_icon05').click(function () {
		$('.img_01').css("margin-left", "-37.5%");
	});

	$('.cos_word').hover(function () {
		$(this).find('p').addClass('active');
		$(this).find('span').addClass('active');
	}, function () {
		$(this).find('p').removeClass('active');
		$(this).find('span').removeClass('active');
	});

	$(window).scroll(function () {
		var $st = $(window).scrollTop();

		if ($st > $('.about_left').offset().top - 800) {
			$('.about_left').addClass('scaleWidth');
		} else {
			$('.about_left').removeClass('scaleWidth');
		}

	});

	//resize
	$(window).on('resize', function () {});

})
