$(function () {
   //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    });
})
