$(function () {
    $(".nav_01").addClass('on');
    $(".nav_01").hover(function () {
        $(".nav_01").addClass('on');
        $(".nav_02,.nav_03").removeClass('on');
        $(".pic_01").css("display", "block");
        $(".pic_02,.pic_03").css("display", "none");
    }, function () {
        $(".nav_01").addClass('on');
        $(".nav_02,.nav_03").removeClass('on');
        $(".pic_01").css("display", "block");
        $(".pic_02,.pic_03").css("display", "none");
        $('.vision_cont .vision_nav div.nav_01::after').css('opacity','1');
    });

    $(".nav_02").hover(function () {
        $(".nav_02").addClass('on');
        $(".nav_01,.nav_03").removeClass('on');
        $(".pic_02").css("display", "block");
        $(".pic_01,.pic_03").css("display", "none");
    }, function () {
        $(".nav_02").addClass('on');
        $(".nav_01,.nav_03").removeClass('on');
        $(".pic_02").css("display", "block");
        $(".pic_01,.pic_03").css("display", "none");
    });

    $(".nav_03").hover(function () {
        $(".nav_03").addClass('on');
        $(".nav_02,.nav_01").removeClass('on');
        $(".pic_03").css("display", "block");
        $(".pic_01,.pic_02").css("display", "none");
    }, function () {
        $(".nav_03").addClass('on');
        $(".nav_02,.nav_01").removeClass('on');
        $(".pic_03").css("display", "block");
        $(".pic_01,.pic_02").css("display", "none");
    });

});
