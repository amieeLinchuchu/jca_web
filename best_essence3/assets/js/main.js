$(function() {
  var $window = $(window);

  /**
   * 判斷螢幕寬度
   */
  var isTable = false;
  var isMobile = false;

  $window.on('resize', function() {
    var ww = $window.width();
    if (ww <= 768){
      isTable = true;
    }

    if (ww <= 512){
      isMobile = true;
    }
  });
  $window.trigger('resize');


  /**
   * 首頁
   */
  function indexInit(){
    // PC版header滾動後置頂
    if (!isTable){
      $window.on('scroll', function() {
        if ($window.scrollTop() >= 200) {
          $('.header').addClass('fixed');
        } else{
          $('.header').removeClass('fixed');
        }
      });
      $window.trigger('scroll');
    }

    // 最上banner 輪播
    var topSlide = new Slide({
      itemSelectors: '.index .banner_top .banner_box .banner_item',
      prevSelectors: '.index .banner_top_arrow .item_prev',
      nextSelectors: '.index .banner_top_arrow .item_next',
      mode: 'opacity',
      duration: 5000,
      isAutoPlay: true
    });

    // 真心推薦 輪播
    var blogSlide = new Slide({
      itemSelectors: '.index .blog .blog_list .blog_item',
      prevSelectors: '.index .blog_arrow .item_prev',
      nextSelectors: '.index .blog_arrow .item_next',
      mode: 'opacity',
      duration: 5000,
      isAutoPlay: true
    });

    // 影音媒體 輪播
    $('.index .video_wrap .slider').slick({
      dots: true,
      centerMode: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 769,
          settings: {
            centerMode: false,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    // 影音媒體 左右漸層遮罩
    var $html = $('<div class="mask"></div>');
    $('.index .video_wrap .slider').append($html);
  }

  // 關於我們
  function aboutInit(){
    new WOW().init();
  }


  /**
   * 系列商品
   **/
  function productInit(){
    // 系列商品PC版圖片高度
    var $pItem = $('.p_list').find($('.p_img'));
    var $itemW = $pItem.width();

    $window.on('resize', function() {
      if (!isMobile){
        $pItem.height($itemW);
      } else{
        $pItem.height('auto');
      }
    });
    $window.trigger('resize')


    // 建議搭配 輪播
    $('.product_suggest .s_list').slick({
      dots: false,
      slidesToShow: 4,
      slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed: 2000
      responsive: [
        {
          breakpoint: 1025,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 769,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 513,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });
  }

  // 櫃點
  function shopInit(){
    // 選擇櫃點demo
    var $shopImg = $('.shop_img');
    var $storeInfo = $('.shop_infor');

    $('select').change(function () {
      var txtCity = "";
      var txtStore = "";

      $( "#select_city option:selected" ).each(function() {
        txtCity += $(this).text();
      });

      $( "#select_store option:selected" ).each(function() {
        txtStore += $(this).text();
      });

      // console.log(txtCity);
      // console.log(txtStore);

      if(txtCity !== '請選擇縣市' && txtStore !== '請選擇門市'){
        $shopImg.addClass('active');
        $storeInfo.css('opacity', '1');
      }
    }).change();
  }

  // 肌膚檢測
  function skintestInit(){
    // var $chooseItem = $('.test_q :radio, .test_q :checkbox');

    // $chooseItem.on('change', function() {
    //   var $this = $(this);
    //   var $chooseBox = $this.parent('.test_q');

    //   if($chooseBox.hasClass('active')){
    //     $chooseBox.removeClass('active');
    //   } else{
    //     $chooseBox.addClass('active');
    //   }
    // });

    // daterangepicker
    $(function () {
      var $birthday = $('.birthday');
      $birthday.daterangepicker({
        showDropdowns: true,
        singleDatePicker: true, //只選擇一個日期
        autoUpdateInput: false,
        locale: {
          format: 'YYYY-MM-DD',
          applyLabel: '套用',
          cancelLabel: '清除',
          fromLabel: '開始',
          toLabel: '結束',
          customRangeLabel: '自訂',
          weekLabel: '週',
          daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
          monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
          firstDay: 1
        }
      }).prop('autocomplete', 'off').prop('readonly', true);
      // 時間選擇器事件
      $birthday.on('apply.daterangepicker', function(ev, picker) {
        var $this = $(picker.element);
        var dateFormat = $this.data('daterangepicker').locale.format;
        var startDate = picker.startDate.format(dateFormat);
        $this.val(startDate);
      });
    });
    // 檢測結果popup demo
    $('.testform_send').click(function(e) {
      e.preventDefault();
      var $this = $(this);

      $('.popup_testresult').fadeIn();
    });
  }

  // 最新消息
  function newsInit(){
    // 最新消息banner輪播
    var newslistSlide = new Slide({
      // 要輪播的內容
      itemSelectors: '.newslist_banner .newslist_banner_wrap .newslist_banner_item',
      // 輪播點點
      indicatorSelectors: '.newslist_banner .newslist_carousel_indicators .item',
      mode: 'opacity',
      duration: 5000,
      isAutoPlay: true
    });
  }

  // 分享頁新增好友demo
  $('.friend_add_box').on('click', function(){

    var friend_form =
    '<div class="share_form_element new_friend_form">' +
      '<div class="remove_sharebox">' +
        '<img src="assets/images/share/remove_icon.png" alt="刪除欄位">' +
      '</div>' +
      '<div class="share_form_item share_form_item_1">' +
        '<span class="share_form_item_name share_form_item_name_1">好友姓名</span>' +
        '<input class="form_elememt_contain input" type="text" name="name_friends[]" placeholder="請輸入好友姓名">' +
      '</div>' +
      '<div class="share_form_item">' +
        '<span class="share_form_item_name">好友手機</span>' +
        '<input class="form_elememt_contain input" type="tel" name="phone_friends[]" placeholder="請輸入好友手機">' +
      '</div>' +
    '</div>';

    var $friendNew = $('.new_friend_form');
    // console.log($friendNew);

    // 最多新增10個好友
    if ($friendNew.length > 9){
      alert('一次最多只能分享給10位好友！');
      return;
    }

    $('#addfriend').append(friend_form);

    // 分享頁刪除好友
    $('.remove_sharebox').on('click', function(){
      var $thisForm = $(this).parent($('.new_friend_form'));
      $thisForm.remove();
      // console.log($(this).parent($('.new_friend_form')));
    });
  });

  // 視窗滾動，物件出現
  function objectVisible() {
    $window.on('scroll.visible', function() {
      var scrollTop = $window.scrollTop();
      var windowH = $window.height();

      var $visibleObject = $('.visible_object');

      if ($visibleObject.length) {
        $visibleObject.each(function(i) {
          if(!$(this).hasClass('visible-show')){
            var bottom_of_object = $(this).offset().top + $(this).height() / 3;
            var bottom_of_window = scrollTop + windowH;

            // 到指定位置時出現
            if (bottom_of_window >= bottom_of_object) {
              $(this).addClass('visible-show');
              // $(this).attr("style", "opacity: 1");
            }
          }
        });
      }
    });
    $window.trigger('scroll.visible');
  }
  objectVisible();

  // 建議搭配少於三個時置中排列
  var $productContain = $('.product_contain');

  $productContain.each(function(i) {
    var $productItem = $(this).find($('.product_item'));
    if($productItem.length < 3){
      $(this).css('justify-content', 'space-around');
    }
  });

  // popup關閉
  var $pop = $('.popup');
  var $popClose = $pop.find($('.popup_close'));

  $popClose.click(function(e) {
    e.preventDefault();

    $pop.fadeOut();
  });

  // 選單
  var $menuOpen = $('.menu_open_box')
  $menuOpen.click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $menu = $('.header .nav');

    // 選單按鈕的切換
    $this.toggleClass('close');

    // 選單內容的切換
    if ($menu.hasClass('active')) {
      $menu.removeClass('active');
    } else {
      $menu.addClass('active');
    }
  });

  // 判斷目前頁面 決定執行的function
  var defaultPageName = 'index';
  var pathArr = location.pathname.split('/');
  var pathLen = pathArr.length;
  var pageName = pathArr[pathLen - 1].replace('.html', '').replace('.php', '');
  if (!pageName) {
    pageName = defaultPageName;
  }

  if (pageName == 'index') {
    indexInit();
  } else if (pageName == 'about') {
    aboutInit();
  } else if (pageName == 'product') {
    productInit();
  } else if (pageName == 'reserve') {
    reserveInit();
  } else if (pageName == 'shop') {
    shopInit();
  } else if (pageName == 'skintest' || pageName == 'skin_chart') {
    skintestInit();
  }
  else if (pageName == 'news') {
    newsInit();
  }
});
