$(function () {

	//彈窗
	$('.btn_video').click(function () {
		$('.popup,.pop_video').fadeIn();
		$(".youtube-video").attr('src', 'https://www.youtube.com/embed/N4R0R_m67TA?autoplay=1&loop=1&rel=0');
	});

	$('.btn_discord').click(function () {
		$('.popup,.pop_img,.discord_id').fadeIn();
	});

	$('.btn_game_id').click(function () {
		$('.popup,.pop_img,.game_id').fadeIn();
	});

	$('.btn_game_name').click(function () {
		$('.popup,.pop_img,.game_name').fadeIn();
	});

	$('.btn_game_list').click(function () {
		$('.popup,.pop_img,.game_list').fadeIn();
	});

	$('.popup_close').click(function () {
		$(".youtube-video").attr('src', ''); //關閉連結設為空直
	});

	$('.btn_play').click(function () {
		$(this).fadeOut();
		$(".live_iframe").attr('src', 'https://www.youtube.com/embed/jnKu4-b2TKw?autoplay=1&rel=0');
	});

	$(".popup_close").click(function () {
		$('.popup,.popup_wrap > div,.pop_img,.pop_img > img').fadeOut();
	});

	//tab1
	$('.tab_01,.step_04').click(function () {
		$('.tab_01').removeClass('active').addClass('active');
		$('.tab_02').removeClass('active');
		$('.person_title,.cont_02').fadeOut(0);
		$('.team_title,.cont_01').fadeIn(0);
	});

	//tab2
	$('.tab_02').click(function () {
		$(this).removeClass('active').addClass('active');
		$('.tab_01').removeClass('active');
		$('.team_title,.cont_01').fadeOut(0);
		$('.person_title,.cont_02').fadeIn(0);
	});

	//more
	$('.more_01').click(function () {
		$(this).fadeOut();
		$('.m_05').fadeIn();
		$('.more_02').css('display', 'block');
	});

	$('.more_02').click(function () {
		$(this).fadeOut();
		$('.m_06').fadeIn();
	});

	//check
	$('input[name="read"]').on('change', function () {
		var $this = $(this);
		if ($this.prop('checked')) {
			$('.check_word').addClass('active');
		} else {
			$('.check_word').removeClass('active');
		}
	});

	//upload

	$('input').on('change', function (e) {
		const file = this.files[0];
		const objectURL = URL.createObjectURL(file);
		$(this).siblings('img').attr('src', objectURL);
	});

	//nav

	//live
	jQuery(window).scroll(function () {
		let scrollTop = jQuery(window).scrollTop() + 100;
		let areaTop = jQuery("#event_01").offset().top;
		let areaBottom = areaTop + jQuery("#event_01").innerHeight();

		if (scrollTop > areaTop && scrollTop < areaBottom) {
			jQuery(".e_01").addClass("nav_active");
		} else {
			jQuery(".e_01").removeClass("nav_active");
		}

	});

	//intro
	jQuery(window).scroll(function () {

		let scrollTop = jQuery(window).scrollTop() + 100;
		let areaTop = jQuery("#event_02").offset().top;
		let areaBottom = areaTop + jQuery("#event_02").innerHeight();
		if (scrollTop > areaTop && scrollTop < areaBottom) {
			jQuery(".e_02").addClass("nav_active");
		} else {
			jQuery(".e_02").removeClass("nav_active");
		}
	});

	//money
	jQuery(window).scroll(function () {

		let scrollTop = jQuery(window).scrollTop() + 100;
		let areaTop = jQuery("#event_03").offset().top;
		let areaBottom = areaTop + jQuery("#event_03").innerHeight();
		if (scrollTop > areaTop && scrollTop < areaBottom) {
			jQuery(".e_03").addClass("nav_active");
		} else {
			jQuery(".e_03").removeClass("nav_active");
		}

	});

	//role
	jQuery(window).scroll(function () {
		let scrollTop = jQuery(window).scrollTop() + 100;
		let areaTop = jQuery(".role").offset().top;
		let areaBottom = areaTop + jQuery(".role").innerHeight();
		if (scrollTop > areaTop && scrollTop < areaBottom) {
			jQuery(".e_04").addClass("nav_active");
		} else {
			jQuery(".e_04,.e_05,.e_06").removeClass("nav_active");
		}
	});

	$('.e_05').click(function () {
		$(".e_04,.e_06").removeClass("nav_active");
		$(this).addClass("nav_active");
	});
	
	$('.e_04').click(function () {
		$(".e_05,.e_06").removeClass("nav_active");
		$(this).addClass("nav_active");
	});
	
	$('.e_06').click(function () {
		$(".e_05,.e_04").removeClass("nav_active");
		$(this).addClass("nav_active");
	});


})
