(function() {
  'use strict';

  /**
   * 幻燈片動態
   * @constructor
   * @param {object} options
   */
  function Carousel(options) {

    if (!(this instanceof Carousel)) {
      return new Carousel(options);
    }

    var self = this;
    // 是否在動畫中
    self.isAnimated = false;
    // 排列方式
    self.centerMode = !!options.centerMode;

    // 手機觸碰區塊
    self.$swipe = $(options.swipeSelectors);
    // 點點
    self.$indicators = $(options.indicatorSelectors);
    // 內容
    self.$box = $(options.boxSelectors);
    self.$target = $(options.targetSelectors);
    self.$item = $(options.itemSelectors);
    // 左右鍵
    self.$prevBtn = $(options.prevSelectors);
    self.$nextBtn = $(options.nextSelectors);

    // 註冊事件
    self.$indicators.on('click.carousel', function(e) {
      e.preventDefault();
      var index = self.$indicators.index($(this));
      self.indicators(index);
    });

    self.$prevBtn.on('click.carousel', function(e) {
      e.preventDefault();
      self.prev();
    });

    self.$nextBtn.on('click.carousel', function(e) {
      e.preventDefault();
      self.next();
    });

    // 手機手指滑動
    // var hammer = new Hammer(self.$swipe[0]);

    // hammer.on('swipeleft', function( /*e*/ ) {
    //   self.next();
    // });

    // hammer.on('swiperight', function( /*e*/ ) {
    //   self.prev();
    // });

    // 隱藏左右鍵
    self.$prevBtn.css('display', 'none');
    self.$nextBtn.css('display', 'none');

    // 輪播循環
    if (self.centerMode) {
      if (self.$item.length >= 3){
        self.cycle();
      } else if (self.$item.length == 2){
        // 顯示左右鍵
        self.$nextBtn.css('display', 'inline-block');
      }
    }

    if (self.$box.width() < self.$item.width() * self.$item.length) {
      self.cycle();
    } else{
      // 如果沒有超過顯示範圍，輪播不作用
      // self.isAnimated = true;
    }

    // 頁面顯示個數
    self.resize();

    // 預設第一個
    if (self.centerMode) {
      if (self.$item.length >= 3){
        self.$item.eq(self.$item.length / 2).addClass('active');
        // 初始位置
        TweenMax.set(self.$target, { x: -(self.$item.width() * (self.$item.length / 2)), y: '-50%', force3D: true });
      } else{
        self.$item.eq(0).addClass('active');
        TweenMax.set(self.$target, { x: 0, y: '-50%', force3D: true });
      }
    } else {
      self.$item.eq(0).addClass('active');
      TweenMax.set(self.$target, { x: 0, y: '-50%', force3D: true });
    }
    self.$indicators.eq(0).addClass('active');
  }


  /**
   * 輪播循環
   */
  Carousel.prototype.cycle = function() {
    var self = this;

    // 總個數超過可顯示範圍，複製一組在後面
    var cloneAll = self.$item.clone(true, true);
    cloneAll.addClass('cloneitem');

    if (!self.$item.hasClass('cloneitem')){
      self.$target.append(cloneAll);
      self.$item = self.$item.add(cloneAll);
    }

    // 顯示左右鍵
    self.$prevBtn.css('display', 'inline-block');
    self.$nextBtn.css('display', 'inline-block');
  };


  /**
   *  上移
   */
   Carousel.prototype.prev = function() {
    var self = this;
    if (self.isAnimated) { return; }
    self.isAnimated = true;

    // 目前選中的
    var $currentItem = self.$item.filter('.active');
    var currentIndex = self.$item.index($currentItem);
    var itemLen = self.$item.length;

    // 如果是第一個，先回到最後一個
    if (self.centerMode && itemLen >= 3) {
      if (currentIndex == 1) {
        self.$target.css("transition", "none");
        currentIndex = itemLen / 2 + 1;

        var moveX = -(self.$item.width() * (itemLen / 2 + 1));
        TweenMax.set(self.$target, { x: moveX, y: '-50%',force3D: true });
      }
    } else{
      if (currentIndex == 0) {
        self.$target.css("transition", "none");
        currentIndex = itemLen / 2;

        var moveX = -(self.$item.width() * (itemLen / 2));
        TweenMax.set(self.$target, { x: moveX, y: '-50%',force3D: true });
      }
    }

    currentIndex--;
    // console.log(currentIndex);

    // 點點選中
    if (currentIndex == itemLen / 2) {
      self.$indicators.eq(0).addClass('active').siblings().removeClass('active');
    } else if (currentIndex == itemLen / 2 + 1) {
      self.$indicators.eq(1).addClass('active').siblings().removeClass('active');
    }
    self.$indicators.eq(currentIndex).addClass('active').siblings().removeClass('active');

    $currentItem.removeClass('active');
    self.$item.eq(currentIndex).addClass('active');

    var moveX = -(self.$item.width() * currentIndex);

    self.$target.css({
      "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
      "transition": "transform 1s"
    });
    TweenMax.set(self.$target, { x: moveX, y: '-50%',force3D: true });

    // 如果正在移動，等動畫執行結束後 => isAnimated才等於false，左右鍵才可以繼續作用
    TweenMax.delayedCall(0.7, function() {
      self.isAnimated = false;
    });

    if (self.centerMode && itemLen == 2){
        if (currentIndex == 0) {
        self.isAnimated = true;
        self.$prevBtn.css('display', 'none');
        self.$nextBtn.css('display', 'inline-block');
      }
    }
  };


  /**
   *  下移
   */
  Carousel.prototype.next = function() {
    var self = this;
    if (self.isAnimated) { return; }
    self.isAnimated = true;

    // 目前選中的
    var $currentItem = self.$item.filter('.active');
    var currentIndex = self.$item.index($currentItem);
    var itemLen = self.$item.length;

    // 如果是最後一個，回到第一個
    if (currentIndex == itemLen / 2 + 1) {
      self.$target.css("transition", "none");
      currentIndex = 1;
      TweenMax.set(self.$target, { x: -(self.$item.width()), force3D: true });
    }

    currentIndex++;

    // 點點選中
    if (currentIndex == itemLen / 2) {
      self.$indicators.eq(0).addClass('active').siblings().removeClass('active');
    } else if (currentIndex == itemLen / 2 + 1) {
      self.$indicators.eq(1).addClass('active').siblings().removeClass('active');
    }
    self.$indicators.eq(currentIndex).addClass('active').siblings().removeClass('active');

    $currentItem.removeClass('active');
    self.$item.eq(currentIndex).addClass('active');

    var moveX = -(self.$item.width() * currentIndex);

    self.$target.css({
      "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
      "transition": "transform 1s"
    });
    TweenMax.set(self.$target, { x: moveX, y: '-50%',force3D: true });


    // 如果正在移動，等動畫執行結束後 => isAnimated才等於false，左右鍵才可以繼續作用
    TweenMax.delayedCall(0.7, function() {
      self.isAnimated = false;
    });

    if (self.centerMode && itemLen == 2){
      if (currentIndex == 1) {
        self.isAnimated = true;
        self.$nextBtn.css('display', 'none');
        self.$prevBtn.css('display', 'inline-block');
      }
    }
  };


  /**
   * 指定
   */
   Carousel.prototype.indicators = function(index) {
    var self = this;
    if (self.isActive) { return false; }

    // 目前選中的
    var $currentDot = self.$indicators.filter('.active');
    var dotIndex = self.$indicators.index($currentDot);
    // console.log(index)
    // console.log(dotIndex)
    self.$indicators.eq(dotIndex).removeClass('active');
    self.$indicators.eq(index).addClass('active');

    // 目前選中的
    var $currentItem = self.$item.filter('.active');
    // var currentIndex = self.$item.index($currentItem);
    var currentIndex = index;
    var itemLen = self.$item.length;

    if (self.centerMode){
      if (itemLen >= 3){
        if (index == 0){
          currentIndex = itemLen / 2;
        }
      } else if (itemLen == 2){
        if (index == 0){
          self.$prevBtn.css('display', 'none');
          self.$nextBtn.css('display', 'inline-block');
        } else{
          self.$prevBtn.css('display', 'inline-block');
          self.$nextBtn.css('display', 'none');
        }
      }
    }

    $currentItem.removeClass('active');
    self.$item.eq(currentIndex).addClass('active');

    var moveX = -(self.$item.width() * currentIndex);

    self.$target.css({
      "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
      "transition": "transform 1s"
    });
    TweenMax.set(self.$target, { x: moveX, y: '-50%',force3D: true });


    // 如果正在移動，等動畫執行結束後 => isAnimated才等於false，左右鍵才可以繼續作用
    TweenMax.delayedCall(0.7, function() {
      self.isAnimated = false;
    });
  };


  /**
   * 頁面顯示個數
   */
   Carousel.prototype.resize = function() {
    var self = this;

    var boxW = self.$box.parent('div').width();
    var itemW = self.$item.width();

    // 小數點無條件捨去，最小值為1
    var itemMax = Math.max(Math.floor(boxW / itemW), 1);
    self.$box.width(itemMax * itemW);

    // var $target = $this.find(self.$target);
    self.$target.width(self.$item.width() * self.$item.length);
  };

  window.Carousel = Carousel;
})();