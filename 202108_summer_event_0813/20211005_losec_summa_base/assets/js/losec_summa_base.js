$(function () {

    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    });

    $(".wrapper select").click(function () {
        $(this).addClass('active');
    });

    $(".btn_enter").click(function () {
        $('.msg_popup').fadeIn();
    });

    $(".popup_close").click(function () {
        $('.popup').fadeOut();
    });

});
