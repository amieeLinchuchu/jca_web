$(function() {

  var $window = $(window);
  var ww = $window.width();

  // 首頁
  function indexInit(){
    // 歡迎popup出現demo
    var $popup = $('.popup');
    // $popup.fadeIn();

    // 最新消息banner輪播
    var newslide = new Slide({
      indicatorSelectors: '.index_carousel_wrap .index_carousel_indicators .item',
      itemSelectors: '.index_carousel_wrap .index_carousel_box .index_carousel_item',
      // mode: 'opacity',
      duration: 5000,
      isAutoPlay: true
    });

    // 最新消息banner水波
    if (ww > 768){
      var $indexNews = $('.index_carousel_img');
      var $newsLen = $indexNews.length;

      for (var i = 1 ; i < $newsLen + 1 ; i++){
        $('.index_carousel_img_' + i).ripples({
          resolution: 512,
          dropRadius: 15,
          perturbance: 0.02,
          interactive: !1  // 取消滑動產生的波紋
        });
      }

      var areaW = $('.index_carousel_img').width();
      var areaH = $('.index_carousel_img').innerHeight();

      // You can add drops programmatically by doing
      // $('body').ripples("drop", x, y, radius, strength);

      function getRandom(min, max){
        return Math.floor( Math.random() * ( max - min + 1 )) + min;
      };

      setInterval(
        function (){
          // 亂數產生漣漪出現位置
          var x = getRandom(1, areaW);
          var y = getRandom(1, areaH);

          for (var i = 1 ; i < $newsLen + 1 ; i++){
            $('.index_carousel_img_' + i).ripples("drop", x, y, 10, 0.02);
          }
        }, 5000
      );
    }

    // 部落客推薦 輪播
    if ($('.blog .blog_wrap').length) {
      var newslide = new Slide({
        indicatorSelectors: '.blog .blog_item .item',
        itemSelectors: '.blog .blog_contain_area',
        mode: 'opacity',
        duration: 5000,
        isAutoPlay: true
      });
      $('.blog_contain_area').css('transition' , 'none');
    }

    // youtube 影片
    carousel();

    // 滾動顯示
    $window.on('scroll.animate', function() {
      // 暢銷排行榜
      $('.hot_product_item').each(function(i) {
        if ($(this).hasClass('visible-show') && $(this).data('isShow') !== 1){
          // console.log('顯示');
          // console.log($(this));
          TweenMax.staggerFromTo($('.hot_product_item.visible-show'), 1, { y: 50, opacity: 0 }, { y: 0, opacity: 1, force3D: true, clearProps: 'transform' }, 0.3);
          $(this).data('isShow', 1);
        }
      });
    }).trigger('scroll.animate');
  }

  // 關於我們
  function aboutInit(){
    // belif歷史SVG動態
    function pathPrepare ($el) {
      var lineLength = $el[0].getTotalLength();
      $el.css("stroke-dasharray", lineLength);
      $el.css("stroke-dashoffset", lineLength);
    }

    var $line0 = $("path#line0");
    var $line1 = $("path#line1");
    var $line2 = $("path#line2");
    var $line3 = $("path#line3");
    var $line4 = $("path#line4");
    var $line5 = $("path#line5");
    var $line6 = $("path#line6");
    var $line7 = $("path#line7");

    // prepare SVG
    pathPrepare($line0);
    pathPrepare($line1);
    pathPrepare($line2);
    pathPrepare($line3);
    pathPrepare($line4);
    pathPrepare($line5);
    pathPrepare($line6);
    pathPrepare($line7);

    // init controller
    var controller = new ScrollMagic.Controller();

    if (ww > 768){
      // build tween
      var tween0 = new TimelineMax()
        .add(TweenMax.to($('.history1_img1'), 0.5, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history1_word'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line0, 0.8, {strokeDashoffset: 0, ease:Linear.easeNone})) // draw line for 0.8
        .add(TweenMax.to($('.history2_word'), 0.5, { x: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history2_img1'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line1, 0.7, {strokeDashoffset: 0, ease:Linear.easeNone}))
        // .add(TweenMax.to($dot, 0.1, {strokeDashoffset: 0, ease:Linear.easeNone}))  // draw dot for 0.1
        // .add(TweenMax.to("path", 1, {stroke: "#33629c", ease:Linear.easeNone}), 0);  // change color during the whole thing

      var tween1 = new TimelineMax()
        .add(TweenMax.to($('.history3_img1'), 0.4, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history3_word'), 0.4, { x: 0, y: 5, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line2, 1, {strokeDashoffset: 0, ease:Linear.easeNone}))
        .add(TweenMax.to($('.history4_img1'), 0.5, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history4_word'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history4_img2'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line3, 0.8, {strokeDashoffset: 0, ease:Linear.easeNone}))
        .add(TweenMax.to($('.history5_img1'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history5_word'), 0.6, { x: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween2 = new TimelineMax()
        .add(TweenMax.to($line4, 1, {strokeDashoffset: 0, ease:Linear.easeNone}))
        .add(TweenMax.to($('.history6_img1'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history6_word'), 0.6, { x: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line5, 0.7, {strokeDashoffset: 0, ease:Linear.easeNone}))
        .add(TweenMax.to($('.history7_word'), 0.6, { x: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history7_img1'), 0.8, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line6, 1, {strokeDashoffset: 0, ease:Linear.easeNone}))
        .add(TweenMax.to($('.history8_word'), 0.8, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history8_img2'), 0.6, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($line7, 1, {strokeDashoffset: 0, ease:Linear.easeNone}))
        .add(TweenMax.to($('.history8_img1'), 0.8, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      // build scene
      var scene1 = new ScrollMagic.Scene({triggerElement: ".history_wrap", duration: 600, tweenChanges: true})
              .setTween(tween0)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene2 = new ScrollMagic.Scene({triggerElement: ".history_3", duration: 700, tweenChanges: true})
              .setTween(tween1)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene3 = new ScrollMagic.Scene({triggerElement: ".history_6", duration: 800, tweenChanges: true})
              .setTween(tween2)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);
    } else{
      // build tween
      var tween0 = new TimelineMax()
        .add(TweenMax.to($('.history_1'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history1_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween1 = new TimelineMax()
        .add(TweenMax.to($('.history_2'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history2_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween2 = new TimelineMax()
        .add(TweenMax.to($('.history_3'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history3_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween3 = new TimelineMax()
        .add(TweenMax.to($('.history_4'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history4_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween4 = new TimelineMax()
        .add(TweenMax.to($('.history_5'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history5_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween5 = new TimelineMax()
        .add(TweenMax.to($('.history_6'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history6_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween6 = new TimelineMax()
        .add(TweenMax.to($('.history_7'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history7_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      var tween7 = new TimelineMax()
        .add(TweenMax.to($('.history_8'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))
        .add(TweenMax.to($('.history8_line'), 0.2, { x: 0, y: 0, scale: 1, opacity: 1, ease: Power1.easeOut, force3D: true}))

      // build scene
      var scene1 = new ScrollMagic.Scene({triggerElement: ".history_wrap", duration: 100})
              .setTween(tween0)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene2 = new ScrollMagic.Scene({triggerElement: ".history1_word", duration: 200, tweenChanges: true})
              .setTween(tween1)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene3 = new ScrollMagic.Scene({triggerElement: ".history2_word", duration: 200, tweenChanges: true})
              .setTween(tween2)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene4 = new ScrollMagic.Scene({triggerElement: ".history3_word", duration: 100, tweenChanges: true})
              .setTween(tween3)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene5 = new ScrollMagic.Scene({triggerElement: ".history4_word", duration: 150, tweenChanges: true})
              .setTween(tween4)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene6 = new ScrollMagic.Scene({triggerElement: ".history5_word", duration: 150, tweenChanges: true})
              .setTween(tween5)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene7 = new ScrollMagic.Scene({triggerElement: ".history6_word", duration: 150, tweenChanges: true})
              .setTween(tween6)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);

      var scene8 = new ScrollMagic.Scene({triggerElement: ".history7_word", duration: 150, tweenChanges: true})
              .setTween(tween7)
              // .addIndicators() // add indicators (requires plugin)
              .addTo(controller);
    }


    // 五大真實
    var $ripples_effect_area = $('.true_bg');

    var areaW = $ripples_effect_area.width();
    var areaH = $ripples_effect_area.innerHeight();

    // You can add drops programmatically by doing
    // $('body').ripples("drop", x, y, radius, strength);

    function getRandom(min, max){
      return Math.floor( Math.random() * ( max - min + 1 )) + min;
    };

    if (ww > 768){
      $ripples_effect_area.ripples({
        resolution: 512,
        dropRadius: 15,
        perturbance: 0.02,
        interactive: !1  // 取消滑動產生的波紋
      });

      setInterval(
        function (){
          // 亂數產生漣漪出現位置
          var x = getRandom(1, areaW);
          var y = getRandom(1, areaH);

          $ripples_effect_area.ripples("drop", x, y, 10, 0.02);
        }, 5000
      );
    } else{
      $ripples_effect_area.ripples({
        resolution: 300,
        dropRadius: 4,
        perturbance: 0.005,
        interactive: !1  // 取消滑動產生的波紋
      });

      setInterval(
        function (){
          // 亂數產生漣漪出現位置
          var x = getRandom(1, areaW);
          var y = getRandom(areaH / 2, areaH);

          $ripples_effect_area.ripples("drop", x, y, 4, 0.005);
        }, 10000
      );
    }

    // 滾動顯示
    // TweenMax.staggerFromTo($('.true_contain.visible-show'), 0.4, { y: 0, opacity: 0 }, { y: 0, opacity: 1, force3D: true, clearProps: 'transform' }, 0.7);
    // TweenMax.staggerFromTo($('.true_contain.visible-show .true_item img'), 0.4, { y: 20, opacity: 0 }, { y: 0, opacity: 1, force3D: true, clearProps: 'transform' }, 0.8);
    // TweenMax.staggerFromTo($('.true_contain.visible-show .true_item p'), 0.6, { y: 20, opacity: 0 }, { y: 0, opacity: 1, force3D: true, clearProps: 'transform' }, 0.8);
    // TweenMax.staggerFromTo($('.true_contain.visible-show .true_line'), 0.5, { y: 0, opacity: 0 }, { y: 0, opacity: 1, force3D: true, clearProps: 'transform' }, 1);
  }

  // 產品頁
  function productInit(){
    // 上面banner輪播
    var newslide = new Slide({
      indicatorSelectors: '.product_banner .product_carousel_indicators .item',
      itemSelectors: '.product_banner .product_banner_wrap .product_banner_item',
      mode: 'opacity',
      duration: 5000,
      isAutoPlay: true
    });
  }

  // 護膚體驗
  function reserveInit(){
    // 內容介紹popup demo
    var $reserve_intro_popup_open = $('.reserve_intro_popup');
    var $reserve_intro_popup = $('.popup_reserve_intro');

    $reserve_intro_popup_open.click(function(e) {
      e.preventDefault();
      var $this = $(this);
      var index = $reserve_intro_popup_open.index($this);
      $reserve_intro_popup.find('.popup_box_reserve').hide().eq(index).show();
      $reserve_intro_popup.fadeIn();
    });

    // 選擇體驗
    var $reserveAll = $('.reserve_intro_box');
    var $reserveChoose = $('input[name="course"]').prop('checked', false);
    $reserveChoose.on('change', function(e) {
      e.preventDefault();
      var $this = $(this);

      var $reserveBox = $this.parents('.reserve_intro_box');

      // 點不同個，原本的效果移除
      if($reserveAll.hasClass('active')){
        $reserveAll.removeClass('active');
        $reserveBox.addClass('active');
      } else{
        $reserveBox.addClass('active');
      }
    });

    // 預約成功popup demo
    var $reserve_submit = $('.testform_send');
    var $reserve_complete_popup = $('.popup_reserve_complete');

    $reserve_submit.click(function(e) {
      e.preventDefault();

      $reserve_complete_popup.fadeIn();
    });
  }

  // 櫃點
  function shopInit(){
    // 選擇櫃點demo
    var $shopImg = $('.shop_img');
    var $storeInfo = $('.shop_infor');

    $('select').change(function () {
      var txtCity = "";
      var txtStore = "";

      $( "#select_city option:selected" ).each(function() {
        txtCity += $(this).text();
      });

      $( "#select_store option:selected" ).each(function() {
        txtStore += $(this).text();
      });

      // console.log(txtCity);
      // console.log(txtStore);

      if(txtCity !== '請選擇縣市' && txtStore !== '請選擇門市'){
        $shopImg.addClass('active');
        $storeInfo.css('opacity', '1');
      }
    }).change();
  }

  // 肌膚檢測
  function skintestInit(){
    // var $chooseItem = $('.test_q :radio, .test_q :checkbox');

    // $chooseItem.on('change', function() {
    //   var $this = $(this);
    //   var $chooseBox = $this.parent('.test_q');

    //   if($chooseBox.hasClass('active')){
    //     $chooseBox.removeClass('active');
    //   } else{
    //     $chooseBox.addClass('active');
    //   }
    // });

    // daterangepicker
    $(function () {
      var $birthday = $('.birthday');
      $birthday.daterangepicker({
        showDropdowns: true,
        singleDatePicker: true, //只選擇一個日期
        autoUpdateInput: false,
        locale: {
          format: 'YYYY-MM-DD',
          applyLabel: '套用',
          cancelLabel: '清除',
          fromLabel: '開始',
          toLabel: '結束',
          customRangeLabel: '自訂',
          weekLabel: '週',
          daysOfWeek: ['日', '一', '二', '三', '四', '五', '六'],
          monthNames: ['一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月'],
          firstDay: 1
        }
      }).prop('autocomplete', 'off').prop('readonly', true);
      // 時間選擇器事件
      $birthday.on('apply.daterangepicker', function(ev, picker) {
        var $this = $(picker.element);
        var dateFormat = $this.data('daterangepicker').locale.format;
        var startDate = picker.startDate.format(dateFormat);
        $this.val(startDate);
      });
    });
    // 檢測結果popup demo
    $('.testform_send').click(function(e) {
      e.preventDefault();
      var $this = $(this);

      $('.popup_testresult').fadeIn();
    });
  }

  // 最新消息
  function newsInit(){
    // 最新消息banner輪播
    var newslistSlide = new Slide({
      // 要輪播的內容
      itemSelectors: '.newslist_banner .newslist_banner_wrap .newslist_banner_item',
      // 輪播點點
      indicatorSelectors: '.newslist_banner .newslist_carousel_indicators .item',
      mode: 'opacity',
      duration: 5000,
      isAutoPlay: true
    });
  }
  
  // 分享頁新增好友demo
  $('.friend_add_box').on('click', function(){

    var friend_form =
    '<div class="share_form_element new_friend_form">' +
      '<div class="remove_sharebox">' +
        '<img src="assets/images/share/remove_icon.png" alt="刪除欄位">' +
      '</div>' +
      '<div class="share_form_item share_form_item_1">' +
        '<span class="share_form_item_name share_form_item_name_1">好友姓名</span>' +
        '<input class="form_elememt_contain input" type="text" name="name_friends[]" placeholder="請輸入好友姓名">' +
      '</div>' +
      '<div class="share_form_item">' +
        '<span class="share_form_item_name">好友手機</span>' +
        '<input class="form_elememt_contain input" type="tel" name="phone_friends[]" placeholder="請輸入好友手機">' +
      '</div>' +
    '</div>';

    var $friendNew = $('.new_friend_form');
    // console.log($friendNew);

    // 最多新增10個好友
    if ($friendNew.length > 9){
      alert('一次最多只能分享給10位好友！');
      return;
    }

    $('#addfriend').append(friend_form);

    // 分享頁刪除好友
    $('.remove_sharebox').on('click', function(){
      var $thisForm = $(this).parent($('.new_friend_form'));
      $thisForm.remove();
      // console.log($(this).parent($('.new_friend_form')));
    });
  });

  // 內容距離header的位置
  $window.on('resize.container', function() {

    var $header = $('header');
    var $container = $('.main_container');

    var headerH = $header.height();
    $container.css('padding-top' , headerH);

  }).trigger('resize.container');

  // 視窗滾動，物件出現
  function objectVisible() {
    $window.on('scroll.visible', function() {
      var scrollTop = $window.scrollTop();
      var windowH = $window.height();

      var $visibleObject = $('.visible_object');

      if ($visibleObject.length) {
        $visibleObject.each(function(i) {
          if(!$(this).hasClass('visible-show')){
            var bottom_of_object = $(this).offset().top + $(this).height() / 3;
            var bottom_of_window = scrollTop + windowH;

            // 到指定位置時出現
            if (bottom_of_window >= bottom_of_object) {
              $(this).addClass('visible-show');
              // $(this).attr("style", "opacity: 1");
            }
          }
        });
      }
    });
    $window.trigger('scroll.visible');
  }
  objectVisible();

  // 左右箭
  function carousel() {
    var $carouselBox = $('.carousel_box');

    $carouselBox.each(function() {
      // 左右鍵
      var $itemBtn = $(this).find('.item_btn_all');
      var $itemPrev = $(this).find('.item_prev');
      var $itemNext = $(this).find('.item_next');

      // var $carouselAllBox = $carouselBox.find('.story_carousel_choose');
      var $carouselAllBox = $(this).find('.carousel_box_contain');

      var $carouselAll = $(this).find('.carousel_box_inner');
      var $Item = $(this).find('.item_box');

      var yearW = $Item.width();
      var yearlen = $Item.length;
      var itemNum = 2; // 頁面上顯示的個數

      if (!$(this).hasClass('no-cycling')){
        // 輪播循環
        // 超過顯示範圍
        if ($carouselAllBox.width() < yearW * yearlen) {
          // 複製一組在後面
          var cloneAll = $Item.clone();
          cloneAll.addClass('cloneitem');
          $carouselAll.append(cloneAll);

          // 左右鍵顯示
          $itemBtn.addClass('active');
        } else {
          // 左右鍵隱藏
          $itemBtn.removeClass('active');
        }

      } else{
        // 輪播不循環
        // 超過顯示範圍
        if ($carouselAllBox.width() < yearW * yearlen) {
          // 左右鍵顯示
          $itemBtn.addClass('active');
        } else {
          // 左右鍵隱藏
          $itemBtn.removeClass('active');
        }
      }

      $Item.eq(0).addClass('active');

      var $carouselAll = $(this).find('.carousel_box_inner');
      var $Item = $(this).find('.item_box');

      var yearlen = $Item.length;
      $carouselAll.css("width", yearlen * yearW);

      var animated = false; // 動畫執行中 按鈕不作用

      $itemNext.on('click', function(e) {
        e.preventDefault();
        if (animated) { return; }

        animated = true;
        dishesNext();
      });

      $itemPrev.on('click', function(e) {
        e.preventDefault();
        if (animated) { return; }

        animated = true;
        dishesPrev();
      });

      $window.on('resize.year', function() {
        var boxW = $(this).width();
        var yearW = $Item.width();
        // 若有小數點則無條件捨去，最小值為1
        itemMax = Math.max(Math.floor(boxW / yearW), 1);
        // itemMin = Math.min(4);
        $carouselAllBox.width(itemMax * yearW);

      }).trigger('resize.year');

      // 如果有no-cycling就不循環
      if ($(this).hasClass('no-cycling')){
        function dishesNext() {
          var $currentYear = $Item.filter('.active');
          var yeari = $Item.index($currentYear);

          yeari++;
          // 防呆
          if (yeari <= yearlen - itemNum) {
            $currentYear.removeClass('active');
            $Item.eq(yeari).addClass('active');

            var yearW = $currentYear.width();
            var yearresultX = -(yearW * yeari);

            $carouselAll.css({
              "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
              "transition": "transform 1s"
            });

            TweenMax.set($carouselAll, { x: yearresultX, force3D: true });
          }

          // 超過個數，向右鍵就隱藏
          if (yeari = yearlen - itemNum){
            $itemNext.fadeOut();
          } else{
            $itemNext.fadeIn();
          }
        }

        // $itemPrev.fadeOut();

        function dishesPrev() {
          var $currentYear = $Item.filter('.active');
          var yeari = $Item.index($currentYear);

          yeari--;
          // 防呆
          if (yeari > 0) {
            $currentYear.removeClass('active');
            $Item.eq(yeari).addClass('active');

            var yearW = $currentYear.width();
            var yearresultX = -(yearW * yeari);

            $carouselAll.css({
              "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
              "transition": "transform 1s"
            });

            TweenMax.set($carouselAll, { x: yearresultX, force3D: true });
          } else{
            $itemPrev.fadeOut();
          }
        }

      } else {
        function dishesNext() {
          var $currentYear = $Item.filter('.active');
          var yeari = $Item.index($currentYear);
          // $carouselAll.width(yearW * yearlen);

          yeari++;
          if (yeari === yearlen / 2 + 1) {
            $carouselAll.css("transition", "none");
            yeari = 1;
            TweenMax.set($carouselAll, { x: 0, force3D: true });
          }

          if (yeari > 0 || yeari < yearlen / 2 + 1) {
            $currentYear.removeClass('active');
            $Item.eq(yeari).addClass('active');

            var yearW = $Item.width();
            var yearresultX = -(yearW * yeari);

            $carouselAll.css({
              "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
              "transition": "transform 1s"
            });
            TweenMax.set($carouselAll, { x: yearresultX, force3D: true });
            // console.log(1);

            // 動畫執行時間１秒，1秒之後animated才等於false
            TweenMax.delayedCall(0.7, function() {
              animated = false;
            });
          } else {
            animated = false;
            // console.log(3);
          }
        }

        function dishesPrev() {
          var $currentYear = $Item.filter('.active');
          var yeari = $Item.index($currentYear);
          var yearlen = $Item.length;
          // console.log(yearlen);

          // 防呆
          if (yeari == 0) {
            $carouselAll.css("transition", "none");
            yeari = yearlen / 2;
            // console.log(yeari);
            var yearW = $currentYear.width();
            var yearresultX = -(yearW * (yearlen / 2));
            TweenMax.set($carouselAll, { x: yearresultX, force3D: true });
          }

          yeari--;
          if (yeari == 0) {
            $carouselAll.css("transition", "none");
            yeari = yearlen / 2;
            console.log(yeari);
            var yearW = $currentYear.width();
            var yearresultX = -(yearW * (yearlen / 2 + 1));
            TweenMax.set($carouselAll, { x: yearresultX, force3D: true });
          }

          if (yeari > 0 && yeari < yearlen / 2 + 1) {
            $currentYear.removeClass('active');
            $Item.eq(yeari).addClass('active');

            var yearW = $currentYear.width();
            var yearresultX = -(yearW * yeari);
            $carouselAll.css({
              "transition-timing-function": "cubic-bezier(0.645, 0.045, 0.355, 1)",
              "transition": "transform 1s"
            });
            TweenMax.set($carouselAll, { x: yearresultX, force3D: true });
            // console.log(1);

            // 動畫執行時間１秒，1秒之後animated才等於false
            // 動畫執行時間１秒，1秒之後animated才等於false
            TweenMax.delayedCall(0.7, function() {
              animated = false;
              // $('.index_news_mask').css("display", "none");
              // console.log(2);
            });
          } else {
            animated = false;
            // console.log(3);
          }
        }
      }
    });
  }

  // 建議搭配少於三個時置中排列
  var $productContain = $('.product_contain');

  $productContain.each(function(i) {
    var $productItem = $(this).find($('.product_item'));
    if($productItem.length < 3){
      $(this).css('justify-content', 'space-around');
    }
  });

  // 水滴背景滾動
  function scrollBg(){
    var $water_drop_front = $('.common_water_front');
    var $water_drop_back = $('.common_water_end');

    var before = $window.scrollTop();

    $window.on('scroll', function() {
      //滾動距離
      var after = $window.scrollTop();
      var dis_front = (after - before) / 8;
      var dis_back = (after - before) / 7;

      disMax_front = Math.floor(dis_front);
      disMax_back = Math.floor(dis_back);

      $water_drop_front.attr("style", "transform: translateY(" + (-disMax_front) + "px)");
      $water_drop_back.attr("style", "transform: translateY(" + (-disMax_back) + "px)");
    });
  }

  var $containerIndex = $('.container_index');
  if (!$containerIndex.hasClass('show')){
    $containerIndex.addClass('show');
    $('.index_banner').addClass('show');
  }

  // popup關閉
  var $pop = $('.popup');
  var $popClose = $pop.find($('.popup_close'));

  $popClose.click(function(e) {
    e.preventDefault();

    $pop.fadeOut();
  });

  // 選單
  var $menuOpen = $('.menu_open_box')
  $menuOpen.click(function(e) {
    e.preventDefault();
    var $this = $(this);
    var $menuItem = $('.menu');

    // 選單按鈕的切換
    $this.toggleClass('close');

    // 選單內容的切換
    if ($menuItem.hasClass('active')) {
      $menuItem.removeClass('active');
    } else {
      $menuItem.addClass('active');
    }
  });

  // 判斷目前頁面 決定執行的function
  var defaultPageName = 'index';
  var pathArr = location.pathname.split('/');
  var pathLen = pathArr.length;
  var pageName = pathArr[pathLen - 1].replace('.html', '').replace('.php', '');
  if (!pageName) {
    pageName = defaultPageName;
  }

  if (pageName == 'index') {
    indexInit();
    scrollBg();
  } else if (pageName == 'about') {
    aboutInit();
    scrollBg();
  } else if (pageName == 'product_1' || pageName == 'product_2' || pageName == 'product_3' || pageName == 'product') {
    productInit();
  } else if (pageName == 'reserve') {
    reserveInit();
  } else if (pageName == 'shop') {
    shopInit();
  } else if (pageName == 'skintest' || pageName == 'skin_chart') {
    skintestInit();
    scrollBg();
  }
  else if (pageName == 'news') {
    newsInit();
  }
});
