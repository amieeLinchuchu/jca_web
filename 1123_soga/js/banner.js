window.onload = function () {
    var banner = document.querySelector(".vision_cont");
    var bannerImg = document.querySelector(".vision_img");
	var pic = bannerImg.querySelectorAll("div");
    var bannerTabs = document.querySelectorAll(".vision_nav div");
    var index = 0;
    var timer;
    pic[index].style.display = "block";
    function slideImg() {
        pic.forEach(function (currentValue, key, array) {
            currentValue.style.display = "none";
        }, this)
        pic[index].style.display = "block";
    }
    function addCtiveClass() {
        bannerTabs.forEach(function (currentValue, key, array) {
            currentValue.className = '';
        }, this)
        bannerTabs[index].className = "on";
    }
    function autoSlide() {
        timer = setInterval(function () {
            index++;
            if (index > bannerTabs.length-1) {
                index = 0;
            }
            addCtiveClass();
            slideImg();
        }, 3000);
    }
    bannerTabs.forEach(function (currentValue, key, array) {
        currentValue.addEventListener("mouseover", function () {
            {
                if (currentValue.className == 'on') {
                    return;
                }
				pic[index].style.display = "none";
        		pic[key].style.display = "block";
                index = key;
                addCtiveClass();
            }
         }, false)
    }, this);
    banner.addEventListener("mouseover", function () {
        clearInterval(timer);
    }, false);
    banner.addEventListener("mouseout", function () {
        autoSlide();
    }, false)
    autoSlide();
}