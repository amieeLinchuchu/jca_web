$(function () {

	var distance;
	var timer;

	//common pages
	// fullpage =============================================
	var myFullpage = new fullpage('#wrapper', {
		anchors: ['firstPage', 'secondPage', '3rdPage', '4thPage'],
		navigation: true,
		navigationPosition: 'right',
		lazyLoad: true,
		scrollBar: true

	})


	autoScroll()

	function autoScroll() {
		timer = setTimeout(function () {
			fullpage_api.moveTo('secondPage', 1);
		}, 10000);
		distance = setTimeout(function () {
			fullpage_api.moveTo('3rdPage', 1);
		}, 20000);

		// 滑鼠滾輪事件
		document.documentElement.onmousewheel = function (event) {
			if (event.deltaY > 0) {
				clearTimeout(timer);
				clearTimeout(distance);
			}
		}
	}

	let startY;
	document.addEventListener("touchstart", function (event) {
		// event.preventDefault();
		startY = event.changedTouches[0].pageY;
	}, {
		passive: false
	})

	let moveEndY;
	document.addEventListener("touchmove", function (event) {
		// event.preventDefault();
		moveEndY = event.changedTouches[0].pageY;
		let Y = moveEndY - startY;

		// 手指往上滑,下滑頁面時停止自動輪播
		if (Y < 0) {
			clearTimeout(timer);
			clearTimeout(distance);
		}
	}, {
		passive: false
	})


	//adding the action to the button
	$(document).on('click', '#moveTo1', function () {
		fullpage_api.moveTo('firstPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});
	$(document).on('click', '#moveTo3', function () {
		fullpage_api.moveTo('3rdPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});
	$(document).on('click', '#moveTo2', function () {
		fullpage_api.moveTo('secondPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});
	$(document).on('click', '#moveTo4', function () {
		fullpage_api.moveTo('4thPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});

	$(document).on('click', '#gotop', function () {
		fullpage_api.moveTo('firstPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});

	$(document).on('click', '#logo', function () {
		fullpage_api.moveTo('4thPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});
	$(document).on('click', '#scroll', function () {
		fullpage_api.moveTo('secondPage', 1);
		clearTimeout(timer);
		clearTimeout(distance);
	});

	$('.footer_').prev().css('opacity', '1');

	$('.btn_close').click(function () {
		$('.pop_inner,.pop').fadeOut();
		$('.pop_inner > img').removeClass('active');
	});

	$('.price_01,.price_02,.price_03,.price_04,.price_05,.price_06,.price_07,.pp_01,.pp_02,.pp_03,.pp_04,.pp_05,.pp_06,.pp_07,.pp_08,.pp_09,.pp_10,.pp_11,.pp_12').click(function () {
		$('.pop_inner,.pop').fadeIn();
		$('.pop_inner > img').removeClass('active');
	});

	$('.price_01').click(function () {
		$('.pop_01').addClass('active');
	});

	$('.price_02').click(function () {
		$('.pop_02').addClass('active');
	});
	$('.price_03').click(function () {
		$('.pop_03').addClass('active');
	});
	$('.price_04').click(function () {
		$('.pop_04').addClass('active');
	});
	$('.price_05').click(function () {
		$('.pop_05').addClass('active');
	});
	$('.price_06').click(function () {
		$('.pop_06').addClass('active');
	});
	$('.price_07').click(function () {
		$('.pop_07').addClass('active');
	});

	$('.pp_01').click(function () {
		$('.ppic_01').addClass('active');
	});

	$('.pp_02').click(function () {
		$('.ppic_02').addClass('active');
	});
	$('.pp_03').click(function () {
		$('.ppic_03').addClass('active');
	});
	$('.pp_04').click(function () {
		$('.ppic_04').addClass('active');
	});
	$('.pp_05').click(function () {
		$('.ppic_05').addClass('active');
	});
	$('.pp_06').click(function () {
		$('.ppic_06').addClass('active');
	});
	$('.pp_07').click(function () {
		$('.ppic_07').addClass('active');
	});
	$('.pp_08').click(function () {
		$('.ppic_08').addClass('active');
	});
	$('.pp_09').click(function () {
		$('.ppic_09').addClass('active');
	});
	$('.pp_10').click(function () {
		$('.ppic_10').addClass('active');
	});
	$('.pp_11').click(function () {
		$('.ppic_11').addClass('active');
	});
	$('.pp_12').click(function () {
		$('.ppic_12').addClass('active');
	});

})


$(document).ready(function() {
  var $slider = $('.center');
  var $progressBar = $('.progress');
  var $progressBarLabel = $( '.slider__label' );
  
  $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
    var calc = ( (nextSlide) / (slick.slideCount-1) ) * 100;
    
    $progressBar
      .css('background-size', calc + '% 100%')
      .attr('aria-valuenow', calc );
    
    $progressBarLabel.text( calc + '% completed' );
  });
  
});
