$(function () {

	$(".pic_wrap").click(function () {
		var imageUrl = $(this).find('img').attr("src");
		$('.pop_wrap').fadeIn(600);
		$('.pop_img').css("background-image", "url(" + imageUrl + ")");
		$('.pop_word').animate({
			scrollTop: 0,
		}, 0);
		$('.pop_box').animate({
			scrollTop: 0,
		}, 0);

	});
	$(".pop_close").click(function () {
		$('.pop_wrap').fadeOut(600);
	});

})
