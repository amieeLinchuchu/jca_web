$(function() {   
    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    }); 
	
	$(".sunscreen_btn_enter").click(function () {
        $('.popup').fadeIn();
    });
	
	$(".popup_wrap").click(function () {
        $('.popup').fadeOut();
    });
    
})