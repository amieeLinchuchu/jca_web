$(function () {

    $("#gotop").click(function(){
		jQuery("html,body").stop(true,false).animate({scrollTop:0},700); //設定回頁面頂端
		return false;	
	});
   $(window).scroll(function() {
        if ( jQuery(this).scrollTop() > 300){ //設定大於300px才顯示浮層
            jQuery('#gotop').fadeIn("fast");
        } else {
            jQuery('#gotop').stop().fadeOut("fast");
        }
    });

    //scroll
	$('a[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top - 70
				}, 600);
				return false;
			}
		}
	});

    $(".btn_hum").click(function () {
        $(this).toggleClass('click');
        $(".nav").toggleClass('click');
    });
    $(".nav_btn a").click(function () {
        $('.btn_hum').removeClass('click');
        $(".nav").removeClass('click');
    });

    $(".e_01").click(function () {
        $('.pop_01,.pop_box').fadeIn();
    });
    $(".e_02").click(function () {
        $('.pop_02,.pop_box').fadeIn();
    });
    $(".e_03").click(function () {
        $('.pop_03,.pop_box').fadeIn();
    });
    
    $(".awards").click(function () {
        $('.pop_04,.pop_box').fadeIn();
    });

    $(".btn_close,.inner_pop a").click(function () {
        $('.pop_box,.pop_01,.pop_02,.pop_03,.pop_04').fadeOut();
    });

});
