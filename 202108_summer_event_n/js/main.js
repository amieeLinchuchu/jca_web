$(function () {

    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    });

    //loading page
    var $loading = $('#loading');
    window.onload = () => {
        $('#game .logo,#game .btn_menu,.inner_game_cont').delay(2500).fadeIn(600);
        $loading.delay(2000).fadeOut(300);
    }
    //頁面淡入
    $('.wrap').delay(100).fadeIn(600);

    //menu
    $(".btn_menu,.btn_close,.btn_01,.btn_02,.btn_03,.btn_04").click(function () {
        $('.menu_wrap').toggleClass('active');
    });
    $(".btn_03").click(function () {
        $('.search').delay(200).fadeIn(300);
    });
    $(".btn_04,.check_word").click(function () {
        $('.rule').delay(200).fadeIn(300);
    });
    $(".btn_close_g,.btn_close_b").click(function () {
        $('.pop_wrap').fadeOut(300);
    });

    //中獎查詢
    $(".btn_enter").click(function () {
        $('.search_01').fadeOut(0);
        $('.search_02').fadeIn(800);
    });

    //產品彈窗
    $('.product_01').click(function () {
        $('.pop_product,.p_01').fadeIn(500);
    });
    $('.product_02').click(function () {
        $('.pop_product,.p_02').fadeIn(500);
    });
    $('.btn_close_info,.btn_get').click(function () {
        $('.pop_product,.p_01,.p_02').fadeOut(500);
    });

    //收藏愛心加總
    var inputValue = 0;
    $(".heartValue").text(inputValue);
    $(".btn_get.before").click(function () {
        inputValue = inputValue + 1;
        $(".heartValue").text(inputValue);
        $(this).fadeOut().next('.after').fadeIn();
        var heartValue = document.getElementById("heartValue").innerHTML;
        if (heartValue == 2) {
            setTimeout("window.location.href = 'game.html'", 800);
        };
    });

    //倒數計時
    var s = 10;
    var tt
    function reciprocal() {
        s -= 1;
        document.getElementById("showtime").innerHTML = s;
        tt = setTimeout(reciprocal, 1000);
        if(s <= 0){
            clearTimeout(tt);
        };
    };
    
    //遊戲點擊
    var clickValue = 0;
    $(".clickValue").text(clickValue);
    $(".h_click").click(function () {
        clickValue = clickValue + 1;
        $(".clickValue").text(clickValue);
        $('.water_01,.water_02').toggleClass('active');
    });
    
    //開始遊戲
    $('.btn_start').click(function () {
        reciprocal();
        $('.game_cont').fadeOut(300);
        $('.hand_01').addClass('turn_left');
        setTimeout(function () {
            $(".h_click,.skin_01,.water_01,.water_02").fadeOut();
            $('.hand_01').removeClass('turn_left').fadeOut(300);
            $('.skin_01').fadeOut(800);
            $('.after_step').delay(800).fadeIn(800).delay(1500).fadeOut();
            $('.hand_02').delay(1600).addClass('active');
            
            //假設點擊了三十次以上，顯示成功
            if( clickValue >= 30 ){
                $('.game_end,.game_end_success').delay(3200).fadeIn();
                $('.game_end_success img').delay(3700).fadeIn(300);
                setTimeout("window.location.href = 'form.html'", 5000);
            }

            //假設點擊次數少於三十次，顯示失敗
            if( clickValue < 30 ){
                $('.game_end,.game_fail').delay(3200).fadeIn();
                $('.game_fail img').delay(3700).fadeIn(300);
            }
        }, 10000);
    });
    
    //表格送出
    $('.btn_ok').click(function () {
        $('.main_form').fadeOut(500);
        $('.finsh_msg').fadeIn(500);
    });






});
