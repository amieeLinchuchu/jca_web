$(function () {

    //scroll
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 400);
                return false;
            }
        }
    });

    //開場動畫
    setTimeout(function () {
        $('.kv_motion').fadeOut(500);
    }, 4500);

    setTimeout(function () {
        $('.kv_motion').css('display', 'none');
    }, 5000);

    $('.wrapper').fadeIn(500).css({
        "opacity": "1",
        "position": "relative",
        "z-index": 1,
    });

    $('.cookie_info').fadeIn(500);

    $(window).scroll(function () {
        if ($(this).scrollTop() > 10) {
            $('.header,.nav_list_right,.report_top_img,.report_nav').addClass('fixed_bar');
        } else {
            $('.header,.nav_list_right,.report_top_img,.report_nav').removeClass('fixed_bar');
        }
    });

    $('.btn_hum,.nav_list_right ul li div').click(function () {
        $(this).toggleClass('click');
    });

    $('.nav_list_right ul li div').click(function () {
        $(this).next().toggleClass('click');
    });

    $('.btn_hum').click(function () {
        $('.nav_list_m,.nav_list_right,.report_nav_link,.report_nav').toggleClass('click');
    });

    $('.btn_agree').click(function () {
        $('.cookie_info').fadeOut(300);
    });

    $('.search').click(function () {
        $('.search_box').fadeIn(300);
    });

    $('.search_box button').click(function () {
        $('.search_box').fadeOut(300);
    });

    $(".read_more").click(function () {
        var nowPage = $(".now").index();
        var nexPage = nowPage + 1;
        $(".more_page > div").removeClass("now").eq(nexPage).fadeIn(800).addClass("now");
    });

    $(".pop_up").click(function () {
        $(".pop").fadeIn(800);
    });

    $(".btn_pop_close").click(function () {
        $(".pop").fadeOut(800);
    });




});
