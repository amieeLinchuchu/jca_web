$(function () {
	var $window = $(window);
	var ww = $window.width();

	//scroll
	$('a[href*="#"]:not([href="#"])').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 400);
				return false;
			}
		}
	});


	$(".wrapper select").click(function () {
		$('.wrapper select').addClass('active');
	});

	$(".btn_enter").click(function () {
		$('.msg_popup').fadeIn();
	});

	$(".popup_close").click(function () {
		$('.popup').fadeOut();
	});

	$(".btn_look").click(function () {
		$('.userrepot').fadeIn();
	});

	//menu
	$(".btn_menu").click(function () {
		$(this).toggleClass('click');
		$('header ul').toggleClass('click');
	});

	/**
	 * 首頁
	 **/
	function indexInit(){
		// 最新消息banner輪播
		var newslide = new Slide({
			indicatorSelectors: '.index_news .dots .item',
			itemSelectors: '.index_news .news_i',
			targetSelectors: '.index_news .news_l',
			duration: 5000,
			isAutoPlay: true,
			isTouch: true
		});

		// 明星商品輪播
		if (ww <= 768){
			var hotslide = new Slide({
				itemSelectors: '.hotsale_wrap .hotsale_item',
				prevSelectors: '.hotsale_wrap .slide_arrow .item_prev',
				nextSelectors: '.hotsale_wrap .slide_arrow .item_next',
				targetSelectors: '.hotsale_wrap .hotsale_list',
				duration: 4000,
				isAutoPlay: true,
				isTouch: true
			});
		}

		// 部落客推薦
		var blogSlide = new Slide({
			indicatorSelectors: '.blog_wrap .dots .item',
			itemSelectors: '.blog_list .blog_item',
			prevSelectors: '.blog_wrap .slide_arrow .item_prev',
			nextSelectors: '.blog_wrap .slide_arrow .item_next',
			mode: 'opacity',
			duration: 5000,
			isAutoPlay: false
		});
	}


	/**
	 * 系列商品
	 **/
	function productInit(){
		// 商品輪播
		if (ww <= 768){
			var allproductslide = new Slide({
				itemSelectors: '.product_list .p_all_wrap .p_all_item',
				prevSelectors: '.product_list .slide_arrow .item_prev',
				nextSelectors: '.product_list .slide_arrow .item_next',
				targetSelectors: '.product_list .p_all_list',
				duration: 4000,
				isAutoPlay: false,
				isTouch: true
			});

			var suggestslide = new Slide({
				itemSelectors: '.suggest_wrap .suggest_list .suggest_item',
				prevSelectors: '.suggest_wrap .slide_arrow .item_prev',
				nextSelectors: '.suggest_wrap .slide_arrow .item_next',
				targetSelectors: '.suggest_wrap .suggest_list',
				duration: 4000,
				isAutoPlay: false,
				isTouch: true
			});
		}
	}


	/**
	 * 好友分享
	**/
	function shareInit(){
		// 新增好友按鈕
		$('.friend_add_btn').on('click', function(){
			var friend_form =
			'<div class="share_form_element share_form_info new_friend_form">' +
			  '<div class="remove_sharebox"></div>' +
			  '<div class="share_form_item">' +
				'<span class="share_form_item_name">好友名稱</span>' +
				'<input class="form_elememt_contain" type="text" name="name_friends[]" placeholder="請輸入好友名稱">' +
			  '</div>' +
			  '<div class="share_form_item">' +
				'<span class="share_form_item_name">好友手機</span>' +
				'<input class="form_elememt_contain" type="tel" name="phone_friends[]" placeholder="請輸入好友手機">' +
			  '</div>' +
			'</div>';

			var $friendNew = $('.new_friend_form');
			// console.log($friendNew);

			// 最多新增10個好友
			if ($friendNew.length > 9){
			  alert('一次最多只能分享給10位好友！');
			  return;
			}

			$('#addfriend').append(friend_form);

			// 分享頁刪除好友
			$('.remove_sharebox').on('click', function(){
			  var $thisForm = $(this).parent($('.new_friend_form'));
			  $thisForm.remove();
			  // console.log($(this).parent($('.new_friend_form')));
			});
		});
	}


	/**
	 * 選單
	 **/
	var $menuOpen = $('.menu_open_box')
	var $subOpen = $('span.nav_i')
	$menuOpen.click(function(e) {
		e.preventDefault();
		var $this = $(this);
		var $menu = $('.header .nav');

		// 選單按鈕的切換
		$this.toggleClass('close');

		// 選單內容的切換
		if ($menu.hasClass('active')) {
			$menu.removeClass('active');
		} else {
			$menu.addClass('active');
		}

		// 隱藏下層選單
		if ($('.nav_i').hasClass('close')) {
			$('.nav_i').removeClass('close');
		}

		if ($('.panel').hasClass('active')) {
			$('.panel').removeClass('active');
		}
	});

	// 下層選單展開
	$subOpen.click(function(e) {
		e.preventDefault();
		var $this = $(this);
		var $subBox = $this.parent($('.nav_l'));
		var $subNav = $subBox.find($('.panel'));

		// 選單按鈕的切換
		$this.toggleClass('close');

		// 選單內容的切換
		if ($subNav.hasClass('active')) {
			$subNav.removeClass('active');
		} else {
			$subNav.addClass('active');
		}
	});

	// 判斷目前頁面 決定執行的function
	var defaultPageName = 'index';
	var pathArr = location.pathname.split('/');
	var pathLen = pathArr.length;
	var pageName = pathArr[pathLen - 1].replace('.html', '').replace('.php', '');
	if (!pageName) {
		pageName = defaultPageName;
	}

	if (pageName == 'index') {
		indexInit();
	} else if (pageName == 'about') {
		aboutInit();
	} else if (pageName == 'product') {
		productInit();
	} else if (pageName == 'share_default') {
		shareInit();
	} else if (pageName == 'shop') {
		shopInit();
	} else if (pageName == 'skin_chart') {
		skintestInit();
	}
	else if (pageName == 'news') {
		// newsInit();
	}
})
