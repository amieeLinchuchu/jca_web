$(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) { //設定大於300px才顯示浮層
            $('.btn_gotop,.btn_buynow').fadeIn("fast");
            $('.btn_buynow').css('display', 'block');
        } else {
            $('.btn_gotop,.btn_buynow').stop().fadeOut("fast");
        }
    });

    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    });
})
