$(function () {
	$('.machine_center,.bar').removeClass('click');
	
	setTimeout(function(){
		$('.machine_center.center_1').addClass('move');
	}, 600);

    $('.btn_play').click(function () {
        $('.center_2').removeClass('click').addClass('click');
        setTimeout(function () {
            $('.bar').addClass('click');
        }, 2000);
    });

    //input
    $('input[name="read"]').on('change', function () {
        var $this = $(this);
        if ($this.prop('checked')) {
            $('.checkmark').addClass('active');
        } else {
            $('.checkmark').removeClass('active');
        }
    });

    //獎項列表
    $('.btn_list').click(function () {
        $('.wrapper').animate({ scrollTop: 0 },'0').css('overflow-y','hidden');
        $('.pop_box').fadeIn(500).animate({ scrollTop: 0 },'0');
        $('.title_list').delay(800).addClass('open');
        $('.gift_list ul').delay(600).fadeIn('1000');
    });
    $('.btn_close').click(function () {
        $('.wrapper').css('overflow-y','scroll');
        $('.pop_box').fadeOut(500).animate({ scrollTop: 0 },'0');
        $('.title_list').removeClass('open');
        $('.gift_list ul').fadeOut(800);
    });
    $('.btn_rule,.btn_notice').click(function () {
        $('.notice_box').fadeIn(500);
        $('.wrapper').css('overflow-y','hidden');
    });

    $('.btn_event').click(function () {
        $('.pop_box.event_cont').fadeIn(500);
        $('.wrapper').css('overflow-y','hidden');
    });

    //填寫完畢資訊
    $('.btn_send').click(function () {
        $('.thank_msg').fadeIn(0);
        $('.confirmation').fadeOut(0);
    });

    //點選兌換之後的樣式
    //$('.btn_take').click(function () {
        //$(this).addClass('after');
        //$(this).parent().addClass('after');
    //});

    //禁止ios彈出鍵盤時，網頁位移
    var u = navigator.userAgent;
    var flag;
    var myFunction;
    var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
    if (isIOS) {
        document.body.addEventListener('focusin', () => {
            flag = true;
            clearTimeout(myFunction);
        })
        document.body.addEventListener('focusout', () => {
            flag = false;
            if (!flag) {
                myFunction = setTimeout(function () {
                    window.scrollTo({
                        top: 0,
                        left: 0,
                        behavior: "smooth"
                    })
                }, 200);
            } else {
                return
            }
        })
    } else {
        return
    };

    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty('--vh', `${vh}px`);

});
